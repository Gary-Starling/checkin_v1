
from dotenv import load_dotenv  # Для .env
from pathlib import Path        # Работа с папками
import os                       # Для открытия определений
import inter_hr_api             # Модуль взаимодействия с api hr
import const                    # Для всех константных значений
from mySQL import DbMySQL       
from pgSQL import DbPgSQL


# Старт

# Получим данные
HR_TOKEN = os.getenv("SELFCHECKIN_SERVICE_TOKEN")
DEBUG = os.getenv("YII_DEBUG")
YII_SCHEME_INFO = os.getenv("YII_SCHEME_INFO")
YII_HOST_API_INFO = os.getenv("YII_HOST_API_INFO")
DATA_BASE = os.getenv("DATABASE_DRIVER")


if HR_TOKEN == None:
    print("Ошибка. Проверьте HR_TOKEN в файле .env")
    exit(1)
elif YII_SCHEME_INFO == None:
    print("Ошибка. Проверьте YII_SCHEME_INFO в файле .env")  
    exit(1)
elif YII_HOST_API_INFO == None:
    print("Ошибка. Проверьте YII_HOST_API_INFO в файле .env")   
    exit(1) 
    

if DATA_BASE == "mysql":
    db = DbMySQL()
    USER_NAME = os.getenv("MYSQL_USER")
    PASS = os.getenv("MYSQL_PASSWORD")
    HOST = os.getenv("MYSQL_HOST")
    PORT = os.getenv("MYSQL_PORT")
    DB_NAME = os.getenv("MYSQL_DATABASE")
else:
    db = DbPgSQL()
    USER_NAME = os.getenv("PGSQL_USER")
    PASS = os.getenv("PGSQL_PASSWORD")
    HOST = os.getenv("PGSQL_HOST")
    PORT = os.getenv("PGSQL_PORT")
    DB_NAME = os.getenv("PGSQL_DATABASE")

db.connect(USER_NAME, PASS, HOST, PORT, DB_NAME)  # Установим соединение с бд

# Логика
id_s = db.select_for_update_id_userId()

# Массив для установки следующего запроса
ids_to_set = []
geo_id = []


#TODO:AFTER TEST chanege all variables to camelCASE!!!!!

# Упаковка для следующих запросов
for userid in id_s:
    ids_to_set.append(str(userid["id"]))
    geo_id.append(str(userid["userId"])) #TODO:Сделать проверку на наличие ключа???
if DEBUG == 'true':
    for i in range(len(ids_to_set)):
        if i % 8 == 0:
            print()
        print("ids_to_set:" + ids_to_set[i], end="  ")  # Распечатка на отправку
    print("*********************************")

ids_to_set = ",".join(ids_to_set)  # Преобразуем в вид '1,2,3' для запроса
geo_id = ",".join(geo_id)  # Аналогично


if ids_to_set:
    db.update_status_where(ids_to_set, const.STATUS_BUSY)
else:  # В ids_to_set пусто, дальше нет смысла работать
    db.conn.close() # Закрыть соединение с БД
    exit(0)


start_shift_data = {"data": []}  # Пустой словарь под отправку на старт смен
end_shift_data   = {"data": []}  # Соответственно на конец смен

res = db.user_should_work_or_no(geo_id)

# Упаковка в словари end_shift_data/start_shift_data информации в 
# нужном формате для выполнения запросов API конец/старт множества смен
for i in res:
    if i["hrId"] is not None:  # Есть запись в таблице user_job, можем завершить смену
        end_shift_data["data"] += [
            {
                "userId": i["userId"],
                "dateTime": str(i["createdAt"]),
            }
        ]
    else:  # Записи нет, это юзеры вне радиуса
        start_shift_data["data"] += [
            {
                "userId": i["userId"],
                "positionId": i["companyObjectPositionId"],
                "dateTime": str(i["createdAt"]),
            }
        ]


if start_shift_data: # Проверка на пустой массив
    status = {"ERR": [], "OK": []}  # пустой словарь с массивами статусов ERR/OK
    d = {}                          # Словарь в который будем писать r.json

    if DEBUG == 'true':  # Распечатка на отправку debug
        inter_hr_api.jprint(start_shift_data)
        print("*********************************")
    # Запрос на отправку множества смен
    r = inter_hr_api.post(start_shift_data, HR_TOKEN, f"{YII_SCHEME_INFO}" + "://"
        + f"{YII_HOST_API_INFO}/v2/company/object-work-shift-user/start-in-bulk")

    if r.status_code == 200:  # OK
        if DEBUG == 'true':  # Распечатка на отправку debug
            print(r.text)
            print("*********************************")
        inter_hr_api.update_status(r.json(), status)
    else:
        print("Response status = " + str(r.status_code))

if end_shift_data:
    if DEBUG == 'true':  # Распечатка на отправку debug
        inter_hr_api.jprint(end_shift_data)
        print("*********************************")

    # Запрос на завершение множества смен
    r = inter_hr_api.post(end_shift_data, HR_TOKEN, f"{YII_SCHEME_INFO}" + "://"
        + f"{YII_HOST_API_INFO}/v2/company/object-work-shift-user/finish-in-bulk")

    if r.status_code == 200:  # OK
        if DEBUG == 'true':
            print(r.text)
            print("*********************************")
        inter_hr_api.update_status(r.json(), status)
    else:
        print("Response status = " + str(r.status_code))

    # Пустые массивы для UPDATE 90/99
    ids_ok = []
    ids_err = []

    for val in id_s:
        for id in status["ERR"]:
            if id == val["userId"]:
                ids_err.append(str(val["id"]))
        for id in status["OK"]:
            if id == val["userId"]:
                ids_ok.append(str(val["id"]))

    ids_ok = ",".join(ids_ok)
    ids_err = ",".join(ids_err)

    if status:
        if status["OK"]:  # проставить статусы успешно отправленным запросам 99
            db.update_status_where(ids_ok, const.STATUS_OK)
        if status["ERR"]:  # при ошибке 90
            db.update_status_where(ids_err, const.STATUS_ERR)


    if DEBUG == 'true':
        print(start_shift_data)
        print(end_shift_data)
        print("*********************************")

db.conn.close()  # Закрыть соединение с БД
