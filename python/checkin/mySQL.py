""" Модуль для работы с mySQL """

import mysql.connector  # Эта функция принимает параметры host, user и password, а возвращает объект MySQLConnection
from mysql.connector import connect, Error
import const  # Для всех константных значений
import os     # Для открытия определений


class DbMySQL:
    conn = None
    debug = os.getenv("YII_DEBUG")
#######################Functions###################################
    """
    Connect к MySQL

    Параметры
    usr_name - имя пользователя; passw - пароль; host_name - имя хоста(пример 127.0.0.1)
    port_n - номер порта; db - имя БД
    Эти данные лежат в .env
    """
    def connect(self, usr_name, passw, host_name, port_n, db):
        try:
            self.conn = mysql.connector.connect(
                user=usr_name,
                password=passw,
                host=host_name,
                port=port_n,
                database=db,
            )
            cursor = self.conn.cursor()
            if self.conn.is_connected():
                print("Server MySQL")  # Распечатать сведения
                print(self.conn.get_server_info(), "\n")
                cursor.execute("SELECT version();")  # Выполнение SQL-запроса
                record = cursor.fetchone()
                print("Connect to - ", record, "\n")
                print(self.conn)
        except (Exception, Error) as error:
            print("Ошибка при работе с Mysql", error)
            exit(1)


        

    """
    Извлечение из БД(нет commit)

    Параметры
    внутри self.conn - созданный при connect() 
    q - sql запрос

    Возращает result в виде массива словарей
    """

    def exec_read_query(self, q):
        if self.debug == 'true':
            print(q)  # debug
        cursor = self.conn.cursor(dictionary=True)
        result = None
        try:
            cursor.execute(q)  # Запрос
            result = cursor.fetchall()  # Вернуть в виде ассоциативного массива
        except (Exception, Error) as error:
            print(f"The error '{error}' occurred")
        return result

    """
    Используется для записи в БД

    Параметры
    внутри self.conn - созданный при connect() 
    q - sql запрос
    После запроса происходит commit изменений

    Возращает result в виде массива словарей
    """

    def exec_query(self, q, fetch):
        if self.debug == 'true':
            print(q)  # debug
        cursor = self.conn.cursor(dictionary=True)
        result = None
        try:
            cursor.execute(q)
            if fetch:
                result = cursor.fetchall()  # Вернуть в виде ассоциативного массива
        except (Exception, Error) as error:
            print(f"The error '{error}' occurred")

        self.conn.commit()  # Фиксируем изменения
        return result 

#######################querys###################################
    # Взять id,userId со status = 0
    def select_for_update_id_userId(self):#TODO: может передавать множество параметров?
        query = f"SELECT id,userId FROM {const.USER_GEO_LOCATION} WHERE status={const.STATUS_FREE} FOR UPDATE"
        return self.exec_read_query(query)
        
    # Установка в user_geo_location статусов
    # ids - это строка в формате "1,2,5,128 ... и т.п"
    # status может быть 0/1/90/99
    def update_status_where(self, ids, status): #TODO:тут лучше передавать статус и поля
        query = f"UPDATE {const.USER_GEO_LOCATION} SET status={status} WHERE id IN ({ids})"
        self.exec_query(query, const.NO_FETCH) #Без выборки


    # Большой запрос на получение <юзеров в радиусе 30М и НЕ на смене> + <вне раиуса 30М и на смене>
    # # заметка "common\\\\modules\\\\company\\\\models\\\\\\\\" именно такой формат
    """ Описание запроса псевдокод 
    users_no_shift AS (
    select userId, companyobjectpositionid, createdat, hr_user_job.id as hrId
    у которых совпадает objid и modelid, у объекта есть адрес 
    WHERE hr_user_geo_location.userid in(список userid которым поставили статус)
    которые внутри радиуса 30м и не на смене (const.py см. 70 строку -> полное запрос в строчку)
    datetimefinish = null
    )
    user_in_shift AS (
    select userId, companyobjectpositionid, createdat, hr_user_job.id as hrId
    у которых совпадает objid и modelid, у объекта есть адрес
    WHERE hr_user_geo_location.userid in(список userid которым поставили статус)   
    которые вне радиуса 30м и на смене  
    datetimefinish = null 
    )
    SELECT * from user_in_shift UNION SELECT * from users_no_shift
    """
    # Таким образом мы получаем словарь, который легко раскидать по условию Даниила if i["hrId"] is not None:
    # 03.30.22 {const.USER_JOB_DATE_FIN} убрано, из select, т.к. не нужно
    # geo_ids - строка в формате "1,2,5,128 ... и т.п", список id среди которых ведётся поиск
    # возращает словарь в виде объединения user_in_shift/users_no_shift
    def user_should_work_or_no(self, geo_ids):
        query = (  # 1 часть users_no_shift AS
            f"""WITH users_no_shift AS (SELECT {const.COMPANY_OBJ_POS_USER_UID},{const.COMPANY_OBJ_POS_USER_OBJ_ID}, {const.COMPANY_OBJ_POS_USER_CR_DATA},{const.USER_JOB_ID} AS hrId\
        FROM {const.COMPANY_OBJ_POS_USER}\
        INNER JOIN {const.USER_GEO_LOCATION}\
            ON {const.USER_GEO_LOCATION_USERID}={const.COMPANY_OBJ_POS_USER_UID}\
        INNER JOIN {const.ADRESS}\
            ON {const.ADRESS_MODEL_CLASS}='common\\\\modules\\\\company\\\\models\\\\\\\\CompanyObject' \
            AND {const.ADRESS_MODEL_ID} = {const.COMPANY_OBJ_POS_USER_COBJ_ID} 
        LEFT JOIN {const.USER_JOB}\
            ON {const.COMPANY_OBJ_POS_USER_UID} = {const.USER_JOB_UID}\
            AND {const.COMPANY_OBJ_POS_USER_OBJ_ID} = {const.USER_JOB_COM_OBJPOS_ID}"""
            + f" WHERE {const.COMPANY_OBJ_POS_USER_UID} in ({geo_ids}) AND "
            + const.IN_RADIUS
            + f" AND ({const.USER_JOB_DATE_FIN} IS NULL) AND ({const.USER_JOB_ID} IS NULL)), "
            # 2 часть user_in_shift AS
            + f"""user_in_shift AS (SELECT {const.COMPANY_OBJ_POS_USER_UID}, {const.COMPANY_OBJ_POS_USER_OBJ_ID},\
            {const.COMPANY_OBJ_POS_USER_CR_DATA},{const.USER_JOB_ID} AS hrId\
        FROM {const.COMPANY_OBJ_POS_USER}\
        INNER JOIN {const.USER_GEO_LOCATION}\
        ON {const.USER_GEO_LOCATION_USERID}={const.COMPANY_OBJ_POS_USER_UID}\
        INNER JOIN {const.ADRESS}\
        ON {const.ADRESS_MODEL_CLASS}='common\\\\modules\\\\company\\\\models\\\\\\\\CompanyObject' AND {const.ADRESS_MODEL_ID} = {const.COMPANY_OBJ_POS_USER_COBJ_ID} 
        LEFT JOIN {const.USER_JOB}\
        ON {const.COMPANY_OBJ_POS_USER_UID} = {const.USER_JOB_UID}\
        AND {const.COMPANY_OBJ_POS_USER_OBJ_ID} = {const.USER_JOB_COM_OBJPOS_ID}"""
            + f" WHERE {const.COMPANY_OBJ_POS_USER_UID} in ({geo_ids}) AND "
            + const.OUT_RADIUS
            + f" AND ({const.USER_JOB_DATE_FIN} IS NULL) \
        AND ({const.USER_JOB_ID} IS NOT NULL))\
        SELECT * from user_in_shift \
        UNION\
        SELECT * from users_no_shift"  # 3 Объединение
        )

        return self.exec_read_query(query)     