import json  # Для работы с json
import requests  # Для работы с запросами


""" Формат отправки данных в json
{
    "data":
    [
        {
            "userId" : 24505,
            "positionId": 1469,
            "dateTime": "2022-01-01 14:20:00"
        },
        {
            "userId" : 4597,
            "positionId": 1469,
            "dateTime": "2022-01-01 14:20:00"
        }
    ]
}
"""


""" Формат данных в Python
user1 = {"userId": 4597, "positionId": 1469, "dateTime": "2022-01-01 14:00:50"}
user2 = {"userId": 24505, "positionId": 1469, "dateTime": "2022-01-01 14:00:50"}
id_send = {"data": [user1, user2]}
"""


def update_status(val_ls_d, current_d):
    """
    В функцию передаётся val_ls_d массив словарей со статусами
    которые нам нужно проставить в текущий пустой current_d словарь и с помощью
    данной функции, обновлять его значения
    подразумевается OK/ERR(99/90) 
    """
    for i in range(len(val_ls_d)):
        if val_ls_d[i]["status"] == 90:  # Ошибка
            current_d["ERR"].append(val_ls_d[i]["userId"])
        elif val_ls_d[i]["status"] == 99:  # Успех
            current_d["OK"].append(val_ls_d[i]["userId"])


def jprint(obj):
    """
    Функция печатает json в удобном формате данные json, сделана для дебага
    """
    text = json.dumps(obj, indent=4)  # json в строку python, отступ 4
    print(text)


def post(data, token, url):

    """
    POST api
    "Content-Type" должен быть обязательно указан, без него не работает
    Функция возращает результат запроса
    """

    headers = {
        "Authorization": "Bearer " + str(token),
        "Content-Type": "application/json",
    }  # Хедеры
    response = requests.post(
        url,
        data=json.dumps(data),
        headers=headers,
    )
    return response
