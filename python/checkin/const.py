''' Список константных значений '''
import os  # Для открытия определений
from dotenv import load_dotenv  # Для .env
from pathlib import Path  # Работа с папками


# Функция дескриптор, для теста и понимания, как работает формула гаверсинусов
# Рассчитывает дистанцию между геоточками
# Её можно сравнивать с радиусом объекта
# Параметры
# x0,y0 широта и долгота объекта, x1,y1 -||- точки, до которой измеряем расстояние
# Возращает расстояние в метрах
# Данная функция переведена в запрос см. строку IN_RADIUS
def in_radius(x0, y0, x1, y1):
    # Радиус земли
    EARTH_RADIUS = 6372795.0  # Берём среднее значение в метрах, ошибка не > 0.5%
    M_PI = 3.141592653589793
    # перевести координаты в радианы
    lat1 = x0 * M_PI / 180.0
    lat2 = x1 * M_PI / 180.0
    lon1 = y0 * M_PI / 180.0
    lon2 = y1 * M_PI / 180.0
    # косинусы и синусы широт и долготы
    cl1 = math.cos(lat1)
    cl2 = math.cos(lat2)
    sl1 = math.sin(lat1)
    sl2 = math.sin(lat2)
    delta = lon2 - lon1
    cdelta = math.cos(delta)
    sdelta = math.sin(delta)
    # вычисления длины большого круга
    y = math.sqrt(pow(cl2 * sdelta, 2) + pow(cl1 * sl2 - sl1 * cl2 * cdelta, 2))
    x = sl1 * sl2 + cl1 * cl2 * cdelta

    ad = math.atan2(y, x)
    dist = ad * EARTH_RADIUS

    return dist

# Открытие переменных сред
current_dir = os.path.abspath(__file__) # Получим абсолютный путь
current_dir = current_dir.strip('const.py') # Вырежем название скрипта
env_path = Path(current_dir + "/../../.env")        # Путь к .env
#print(env_path)
load_dotenv(dotenv_path=env_path) # Загрузим все переменные окружения
DATA_BASE = os.getenv("DATABASE_DRIVER") 

if DATA_BASE == "mysql":    # Выберем префикс в зависимости от типа драйвера
    TABLE_PREFIX = os.getenv("MYSQL_TABLE_PREFIX") + "_"
else:
    TABLE_PREFIX = os.getenv("PGSQL_TABLE_PREFIX") + "_"


# для запросов
NO_FETCH = 0
ON_FETCH = 1   

# Возможные статусы user_geolocation.status
STATUS_ERR = 90
STATUS_OK = 99
STATUS_FREE = 0
STATUS_BUSY = 1


# Константы названий таблиц


# user_geo_location
USER_GEO_LOCATION = TABLE_PREFIX + "user_geo_location"
USER_GEO_LOCATION_LAT = TABLE_PREFIX + "user_geo_location.latitude"
USER_GEO_LOCATION_LON = TABLE_PREFIX + "user_geo_location.longitude"
USER_GEO_LOCATION_USERID = TABLE_PREFIX + "user_geo_location.userId"

# address
ADRESS = TABLE_PREFIX + "address"
ADRESS_LON = TABLE_PREFIX + "address.longitude"
ADRESS_LAT = TABLE_PREFIX + "address.latitude"
ADRESS_MODEL_CLASS = TABLE_PREFIX + "address.modelClass"
ADRESS_MODEL_ID = TABLE_PREFIX + "address.modelId"

# company_object_position_user
COMPANY_OBJ_POS_USER = TABLE_PREFIX + "company_object_position_user"
COMPANY_OBJ_POS_USER_UID = TABLE_PREFIX + "company_object_position_user.userId"
COMPANY_OBJ_POS_USER_ID = TABLE_PREFIX + "company_object_position_user.id"
COMPANY_OBJ_POS_USER_OBJ_ID = TABLE_PREFIX + "company_object_position_user.companyObjectPositionId"
COMPANY_OBJ_POS_USER_COBJ_ID = TABLE_PREFIX + "company_object_position_user.companyObjectId"
COMPANY_OBJ_POS_USER_CR_DATA = TABLE_PREFIX + "company_object_position_user.createdAt"


# user_job
USER_JOB = TABLE_PREFIX + "user_job"
USER_JOB_UID = TABLE_PREFIX + "user_job.userId"
USER_JOB_DATE_FIN = TABLE_PREFIX + "user_job.dateTimeFinish"
USER_JOB_DATE_STR = TABLE_PREFIX + "user_job.dateTimeStart"
USER_JOB_CREATEDAT = TABLE_PREFIX + "user_job.createdAt"
USER_JOB_COM_OBJPOS_ID = TABLE_PREFIX + "user_job.companyObjectPositionId"
USER_JOB_ID = TABLE_PREFIX + "user_job.id"

# Сложная формула, поэтому вынес отдельно, чтобы было удобнее модифицировать
# Внутри радиуса 30М
IN_RADIUS = f"""(((atan2(sqrt(pow((cos({USER_GEO_LOCATION_LAT} * 3.141592653589793 / 180.0)) * sin(({USER_GEO_LOCATION_LON} * 3.141592653589793 / 180.0) - ({ADRESS_LON} * 3.141592653589793 / 180.0)), 2.0)
    + pow((cos({ADRESS_LAT} * 3.141592653589793 / 180.0)) * (sin({USER_GEO_LOCATION_LAT} * 3.141592653589793 / 180.0)) - (sin({ADRESS_LAT}  * 3.141592653589793 / 180.0)) * (cos({USER_GEO_LOCATION_LAT} * 3.141592653589793 / 180.0))
    * cos(({USER_GEO_LOCATION_LON} * 3.141592653589793 / 180.0) - ({ADRESS_LON} * 3.141592653589793 / 180.0)), 2.0)),
    (sin({ADRESS_LAT}  * 3.141592653589793 / 180.0)) * (sin({USER_GEO_LOCATION_LAT} * 3.141592653589793 / 180.0)) + (cos({ADRESS_LAT}  * 3.141592653589793 / 180.0)) * (cos({USER_GEO_LOCATION_LAT} * 3.141592653589793 / 180.0))
    * cos(({USER_GEO_LOCATION_LON} * 3.141592653589793 / 180.0) - ({ADRESS_LON} * 3.141592653589793 / 180.0)))) * 6372795.0) < 30.0)"""
# Вне радиуса 30М
OUT_RADIUS = f"""(((atan2(sqrt(pow((cos({USER_GEO_LOCATION_LAT} * 3.141592653589793 / 180.0)) * sin(({USER_GEO_LOCATION_LON} * 3.141592653589793 / 180.0) - ({ADRESS_LON} * 3.141592653589793 / 180.0)), 2.0)
    + pow((cos({ADRESS_LAT} * 3.141592653589793 / 180.0)) * (sin({USER_GEO_LOCATION_LAT} * 3.141592653589793 / 180.0)) - (sin({ADRESS_LAT}  * 3.141592653589793 / 180.0)) * (cos({USER_GEO_LOCATION_LAT} * 3.141592653589793 / 180.0))
    * cos(({USER_GEO_LOCATION_LON} * 3.141592653589793 / 180.0) - ({ADRESS_LON} * 3.141592653589793 / 180.0)), 2.0)),
    (sin({ADRESS_LAT}  * 3.141592653589793 / 180.0)) * (sin({USER_GEO_LOCATION_LAT} * 3.141592653589793 / 180.0)) + (cos({ADRESS_LAT}  * 3.141592653589793 / 180.0)) * (cos({USER_GEO_LOCATION_LAT} * 3.141592653589793 / 180.0))
    * cos(({USER_GEO_LOCATION_LON} * 3.141592653589793 / 180.0) - ({ADRESS_LON} * 3.141592653589793 / 180.0)))) * 6372795.0) > 30.0)"""
#TODO:Возможно понадобится некий гистерезис, для отсутствия ложных срабатываний


# Описание большого запроса в виде строки
'''WITH users_no_shift AS
    (SELECT hr_company_object_position_user.userid, hr_company_object_position_user.companyobjectpositionid, hr_user_job.createdat, hr_user_job.id as hrId
FROM hr_company_object_position_user
    INNER JOIN hr_user_geo_location ON hr_user_geo_location.userid=hr_company_object_position_user.userid
    INNER JOIN hr_address ON hr_address.modelClass='common\modules\company\models\\CompanyObject' AND hr_address.modelId = hr_company_object_position_user.companyObjectId
LEFT JOIN hr_user_job
    ON hr_company_object_position_user.userid = hr_user_job.userid
    AND hr_company_object_position_user.companyobjectpositionid = hr_user_job.companyobjectpositionid
WHERE hr_company_object_position_user.userid in (4392,4393,4396,4397,4398,4399,4400,4401,4402,4404,4407,4597,4598,4599,4600,5975,5976,24505) AND (((atan2(sqrt(pow((cos(hr_user_geo_location.latitude * 3.141592653589793 / 180.0)) * sin((hr_user_geo_location.longitude * 3.141592653589793 / 180.0) - (hr_address.longitude * 3.141592653589793 / 180.0)), 2.0)
    + pow((cos(hr_address.latitude * 3.141592653589793 / 180.0)) * (sin(hr_user_geo_location.latitude * 3.141592653589793 / 180.0)) - (sin(hr_address.latitude  * 3.141592653589793 / 180.0)) * (cos(hr_user_geo_location.latitude * 3.141592653589793 / 180.0))
    * cos((hr_user_geo_location.longitude * 3.141592653589793 / 180.0) - (hr_address.longitude * 3.141592653589793 / 180.0)), 2.0)),
    (sin(hr_address.latitude  * 3.141592653589793 / 180.0)) * (sin(hr_user_geo_location.latitude * 3.141592653589793 / 180.0)) + (cos(hr_address.latitude  * 3.141592653589793 / 180.0)) * (cos(hr_user_geo_location.latitude * 3.141592653589793 / 180.0))
    * cos((hr_user_geo_location.longitude * 3.141592653589793 / 180.0) - (hr_address.longitude * 3.141592653589793 / 180.0)))) * 6372795.0) < 30.0) AND (hr_user_job.datetimefinish IS NULL)),
     user_in_shift AS
    (SELECT hr_company_object_position_user.userid, hr_company_object_position_user.companyobjectpositionid, hr_user_job.createdat, hr_user_job.id as hrId
    FROM hr_company_object_position_user
    INNER JOIN hr_user_geo_location ON hr_user_geo_location.userid=hr_company_object_position_user.userid
    INNER JOIN hr_address ON hr_address.modelClass='common\modules\company\models\\CompanyObject' AND hr_address.modelId = hr_company_object_position_user.companyObjectId
LEFT JOIN hr_user_job
    ON hr_company_object_position_user.userid = hr_user_job.userid
    AND hr_company_object_position_user.companyobjectpositionid = hr_user_job.companyobjectpositionid
WHERE hr_company_object_position_user.userid in (4392,4393,4396,4397,4398,4399,4400,4401,4402,4404,4407,4597,4598,4599,4600,5975,5976,24505) AND (((atan2(sqrt(pow((cos(hr_user_geo_location.latitude * 3.141592653589793 / 180.0)) * sin((hr_user_geo_location.longitude * 3.141592653589793 / 180.0) - (hr_address.longitude * 3.141592653589793 / 180.0)), 2.0)
    + pow((cos(hr_address.latitude * 3.141592653589793 / 180.0)) * (sin(hr_user_geo_location.latitude * 3.141592653589793 / 180.0)) - (sin(hr_address.latitude  * 3.141592653589793 / 180.0)) * (cos(hr_user_geo_location.latitude * 3.141592653589793 / 180.0))
    * cos((hr_user_geo_location.longitude * 3.141592653589793 / 180.0) - (hr_address.longitude * 3.141592653589793 / 180.0)), 2.0)),
    (sin(hr_address.latitude  * 3.141592653589793 / 180.0)) * (sin(hr_user_geo_location.latitude * 3.141592653589793 / 180.0)) + (cos(hr_address.latitude  * 3.141592653589793 / 180.0)) * (cos(hr_user_geo_location.latitude * 3.141592653589793 / 180.0))
    * cos((hr_user_geo_location.longitude * 3.141592653589793 / 180.0) - (hr_address.longitude * 3.141592653589793 / 180.0)))) * 6372795.0) > 30.0) AND (hr_user_job.datetimefinish IS NULL) AND (hr_user_job.id IS NOT NULL))
SELECT * from user_in_shift UNION SELECT * from users_no_shift;'''
